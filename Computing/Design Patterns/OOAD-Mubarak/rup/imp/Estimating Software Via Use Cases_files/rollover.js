function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}

    // sam - refresh appropriate "over" graphic
    updateOverGraphic();
}

var preloadFlag = false;
function preloadImages() {
	if (document.images) {
		home_over = newImage("http://www.ironspeed.com/images/home-over_g.gif");
		home_on = newImage("http://www.ironspeed.com/images/home-over_g.gif");
		products_over = newImage("http://www.ironspeed.com/images/products-over_g.gif");
		products_on = newImage("http://www.ironspeed.com/images/products-over_g.gif");
		support_over = newImage("http://www.ironspeed.com/images/support-over_g.gif");
		support_on = newImage("http://www.ironspeed.com/images/support-over_g.gif");
		company_over = newImage("http://www.ironspeed.com/images/company-over_g.gif");
		company_on = newImage("http://www.ironspeed.com/images/company-over_g.gif");
		contact_over = newImage("http://www.ironspeed.com/images/contact-over_g.gif");
		contact_on = newImage("http://www.ironspeed.com/images/contact-over_g.gif");
		preloadFlag = true;
		
		// sam - refresh appropriate "over" graphic
    	updateOverGraphic();
	}
}

// sam - added to display appropriate "over" graphic for corresponding active section page
function updateOverGraphic() {
				var URLArray = document.URL.split('/');
				var directory;
				if (URLArray.length > 1) {
					directory = URLArray[URLArray.length - 2];
				}
				else {
					directory = URLArray[URLArray.length - 1];
				}

				/* 
				var filenamePlusAnchorString = URLArray[URLArray.length - 1];
				var filenamePlusAnchorArray = filenamePlusAnchorString.split('#');
				var filename;
				if (filenamePlusAnchorArray.length > 1) {
					filename = filenamePlusAnchorArray[filenamePlusAnchorArray.length - 2];
					}
				else {
					filename = filenamePlusAnchorArray[filenamePlusAnchorArray.length - 1];
					}
				var filenameBeginning = filename.substring(0,4);
				*/
				
		  		switch (directory) {
		  			case '':
		  				document['home'].src = "http://www.ironspeed.com/images/home-over_g.gif";
		  				break;
					case 'products':
		  				document['products'].src = "http://www.ironspeed.com/images/products-over_g.gif";
		  				break;
					case 'support':
		  				document['support'].src = "http://www.ironspeed.com/images/support-over_g.gif";
		  				break;
					case 'company':
		  				document['company'].src = "http://www.ironspeed.com/images/company-over_g.gif";
		  				break;
					case 'contact':
		  				document['contact'].src = "http://www.ironspeed.com/images/contact-over_g.gif";
		  				break;
		  		}
}

