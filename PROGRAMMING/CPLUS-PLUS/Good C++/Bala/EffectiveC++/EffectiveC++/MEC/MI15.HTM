<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN" "http://www.w3.org/TR/REC-html40/frameset.dtd">
<HTML LANG="EN">
<HEAD>
<TITLE>More Effective C++ | Item 15: Understand the costs of exception handling</TITLE>
<LINK REL=STYLESHEET HREF=../INTRO/ECMEC.CSS>

<SCRIPT LANGUAGE="Javascript" SRC="../JAVA/COOKIE.JS"></SCRIPT>
<SCRIPT LANGUAGE="Javascript">var imagemax = 0; setCurrentMax(0);</SCRIPT>
<SCRIPT LANGUAGE="Javascript" SRC="../JAVA/DINGBATS.JS"></SCRIPT>
<SCRIPT>
var dingbase = "MI15_DIR.HTM";
var dingtext = "Item M15, P";
if (self == top) {
 top.location.replace(dingbase + this.location.hash);
}
</SCRIPT>

</HEAD>
<BODY BGCOLOR="#FFFFFF" TEXT="#000000" ONLOAD="setResize()">
<!-- SectionName = "M15: Understand the costs of exception handling" -->
<A NAME="40989"></A>
<DIV ALIGN="CENTER"><FONT SIZE="-1">Back to <A HREF="./MI14_FR.HTM" TARGET="_top">Item 14: Use exception specifications judiciously</A> &nbsp;&nbsp;<BR>&nbsp;&nbsp;Continue to <A HREF="./MEFFICFR.HTM" TARGET="_top">Efficiency</A></FONT></DIV>


<P><A NAME="dingp1"></A><font ID="mititle">Item 15: &nbsp;Understand the costs of exception handling.</font><SCRIPT>create_link(1);</SCRIPT>
</P>

<A NAME="72140"></A><A NAME="40992"></A>
<P><A NAME="dingp2"></A>
To handle exceptions at runtime, programs must do a fair amount of bookkeeping. At each point during execution, they must be able to identify the objects that require destruction if an exception is thrown; they must make note of each entry to and exit from a <CODE>try</CODE> block; and for each <CODE>try</CODE> block, they must keep track of the associated <CODE>catch</CODE> clauses and the types of exceptions those clauses can handle. This bookkeeping is not free. Nor are the runtime comparisons necessary to ensure that exception specifications are satisfied. Nor is the work expended to destroy the appropriate objects and find the correct <CODE>catch</CODE> clause when an exception is thrown. No, exception handling has costs, and you pay at least some of them even if you never use the keywords <CODE>try</CODE>, <CODE>throw</CODE>, or <CODE>catch</CODE>.<SCRIPT>create_link(2);</SCRIPT>
</P><A NAME="83681"></A>
<P><A NAME="dingp3"></A>
Let us begin with the things you pay for even if you never use any exception-handling features. You pay for the space used by the data structures needed to keep track of which objects are fully constructed (see <A HREF="./MI10_FR.HTM#38223" TARGET="_top">Item 10</A>), and you pay for the time needed to keep these data structures up to date. These costs are typically quite modest. Nevertheless, programs compiled without support for exceptions are typically both faster and smaller than their counterparts compiled with support for <NOBR>exceptions.<SCRIPT>create_link(3);</SCRIPT>
</NOBR></P><A NAME="83685"></A>
<P><A NAME="dingp4"></A>
In theory, you don't have a choice about these costs: exceptions are part of C++, compilers have to support them, and that's that. You can't even expect compiler vendors to eliminate the costs if you use no exception-handling features, because programs are typically composed of multiple independently generated object files, and just because one object file doesn't do anything with exceptions doesn't mean others <A NAME="p79"></A>don't. Furthermore, even if none of the object files linked to form an executable use exceptions, what about the libraries they're linked with? If <I>any part</I> of a program uses exceptions, the rest of the program must support them, too. Otherwise it may not be possible to provide correct exception-handling behavior at <NOBR>runtime.<SCRIPT>create_link(4);</SCRIPT>
</NOBR></P><A NAME="66173"></A>
<P><A NAME="dingp5"></A>
That's the theory. In practice, most vendors who support exception handling allow you to control whether support for exceptions is included in the code they generate. If you know that no part of your program uses <CODE>try</CODE>, <CODE>throw</CODE>, or <CODE>catch</CODE>, and you also know that no library with which you'll link uses <CODE>try</CODE>, <CODE>throw</CODE>, or <CODE>catch</CODE>, you might as well compile without exception-handling support and save yourself the size and speed penalty you'd otherwise probably be assessed for a feature you're not using. As time goes on and libraries employing exceptions become more common, this strategy will become less tenable, but given the current state of C++ software development, compiling without support for exceptions is a reasonable performance optimization if you have already decided not to use exceptions. It may also be an attractive optimization for libraries that eschew exceptions, provided they can guarantee that exceptions thrown from client code never propagate into the library. This is a difficult guarantee to make, as it precludes client redefinitions of library-declared virtual functions; it also rules out client-defined callback <NOBR>functions.<SCRIPT>create_link(5);</SCRIPT>
</NOBR></P><A NAME="66049"></A>
<P><A NAME="dingp6"></A>
A second cost of exception-handling arises from <CODE>try</CODE> blocks, and you pay it whenever you use one, i.e., whenever you decide you want to be able to catch exceptions. Different compilers implement <CODE>try</CODE> blocks in different ways, so the cost varies from compiler to compiler. As a rough estimate, expect your overall code size to increase by 5-10% and your runtime to go up by a similar amount if you use <CODE>try</CODE> blocks. This assumes no exceptions are thrown; what we're discussing here is just the cost of <I>having</I> <CODE>try</CODE> blocks in your programs. To minimize this cost, you should avoid unnecessary <CODE>try</CODE> <NOBR>blocks.<SCRIPT>create_link(6);</SCRIPT>
</NOBR></P><A NAME="66270"></A>
<P><A NAME="dingp7"></A>
Compilers tend to generate code for exception specifications much as they do for <CODE>try</CODE> blocks, so an exception specification generally incurs about the same cost as a <CODE>try</CODE> block. Excuse me? You say you thought exception specifications were just specifications, you didn't think they generated code? Well, now you have something new to think <NOBR>about.<SCRIPT>create_link(7);</SCRIPT>
</NOBR></P><A NAME="66269"></A>
<P><A NAME="dingp8"></A>
Which brings us to the heart of the matter, the cost of throwing an exception. In truth, this shouldn't be much of a concern, because exceptions should be rare. After all, they indicate the occurrence of events that are <I>exceptional</I>. The 80-20 rule (see <A HREF="./MI16_FR.HTM#40995" TARGET="_top">Item 16</A>) tells us that such events should almost never have much impact on a program's overall performance. Nevertheless, I know you're curious about just how big a hit you'll take if you throw an exception, and the answer is it's proba<A NAME="p80"></A>bly a big one. Compared to a normal function return, returning from a function by throwing an exception may be as much as <I>three orders of magnitude</I> slower. That's quite a hit. But you'll take it only if you throw an exception, and that should be almost never. If, however, you've been thinking of using exceptions to indicate relatively common conditions like the completion of a data structure traversal or the termination of a loop, now would be an excellent time to think <NOBR>again.<SCRIPT>create_link(8);</SCRIPT>
</NOBR></P><A NAME="66274"></A>
<P><A NAME="dingp9"></A>
But wait. How can I know this stuff? If support for exceptions is a relatively recent addition to most compilers (it is), and if different compilers implement their support in different ways (they do), how can I say that a program's size will generally grow by about 5-10%, its speed will decrease by a similar amount, and it may run orders of magnitude slower if lots of exceptions are thrown? The answer is frightening: a little rumor and a handful of benchmarks (see <A HREF="./MI23_FR.HTM#41253" TARGET="_top">Item 23</A>). The fact is that most people &#151; including most compiler vendors &#151; have little experience with exceptions, so though we know there are costs associated with them, it is difficult to predict those costs <NOBR>accurately.<SCRIPT>create_link(9);</SCRIPT>
</NOBR></P><A NAME="66368"></A>
<P><A NAME="dingp10"></A>
The prudent course of action is to be aware of the costs described in this item, but not to take the numbers very seriously. Whatever the cost of exception handling, you don't want to pay any more than you have to. To minimize your exception-related costs, compile without support for exceptions when that is feasible; limit your use of <CODE>try</CODE> blocks and exception specifications to those locations where you honestly need them; and throw exceptions only under conditions that are truly exceptional. If you still have performance problems, profile your software (see <A HREF="./MI16_FR.HTM#40995" TARGET="_top">Item 16</A>) to determine if exception support is a contributing factor. If it is, consider switching to different compilers, ones that provide more efficient implementations of C++'s exception-handling <NOBR>features.<SCRIPT>create_link(10);</SCRIPT>
</NOBR></P>
<DIV ALIGN="CENTER"><FONT SIZE="-1">Back to <A HREF="./MI14_FR.HTM" TARGET="_top">Item 14: Use exception specifications judiciously</A> &nbsp;&nbsp;<BR>&nbsp;&nbsp;Continue to <A HREF="./MEFFICFR.HTM" TARGET="_top">Efficiency</A></FONT></DIV>

</BODY>
</HTML>
