// Sends users to the page they type into the GO field
// Value passed is the page number
// Scripts are based of Page ranges loaded into one array for each section of the book
// Roman Numerals and the Intro are in seprate sets of code because I could not build the link
// for intro and roman numerials were requested after the first section was done.
// make sure all check variables before editing script.  -JD

function goTo(value)
{value = value.toLowerCase();
var chunks = getCookie("chunk");
if(chunks == "Chapter"){echapter(value);}
else if(chunks == "Item"){eitem(value);} 
else if(chunks == "Book"){ebook(value);}
}


function echapter(value)
{
var Found = "No";
	var EC1 = [13,21];
	var EC2 = [22,48];
	var EC3 = [49,76];
	var EC4 = [77,122];
	var EC5 = [123,152];
	var EC6 = [153,211];
	var EC7 = [212,236];
	var rom1 = ['xiii','xiv','xv','xvi'];
	var rom2 = ['xvii','xviii','xix','xx'];
	var rom3 = ['vii','viii'];
	var rom4 = ['ix','x','xi','xii'];
	var rom5 = [237,238];
	var rom6 = [1,12];
	var romloc1 = new Array("ECPREFFR.HTM","ECPREFAC.HTM");
	var romloc2 = new Array("ECACK_FR.HTM","ECACK.HTM");
	var romloc3 = new Array("EDED_FR.HTM","EDED.HTM");
	var romloc4 = new Array("INDEX.HTM","ETOC.HTM");
	var romloc5 = new Array("ECAFTRFR.HTM","ECAFTER.HTM");
	var romloc6 = new Array("ECINTRFR.HTM","ECINTRO.HTM");

		var aryname = "EC";
			for (ary = 1; ary <=7 ; ary++){
					var arrayname = eval(aryname+ary);
					if (value >=arrayname[0] && value <= arrayname[1]){
							var Found = "Yes";
							var url = new RegExp("EC"+ary+"_FR.HTM");
							var location = top.location.href;
							if (location.search(url) != -1)
								{top.location.hash = "p"+value;
								 setTimeout(top.TEXT.location.hash = "p"+value,500);
								}
							else
								{
								// Check Added because IE does not like Timeout and
								//Netscape needs the Timeout
								// to avoid refresh+run problem cause a GPF
									if (navigator.appName == 'Netscape')  {
								setTimeout(top.location.href = "EC"+ary+"_FR.HTM#p"+value,20)}
								else{top.location.href = "EC"+ary+"_FR.HTM#p"+value;}
								}
							}
						}

					var aryname = "rom";
					for (ary = 1; ary <=5 ; ary++){
						var arrayname = eval(aryname+ary);

						for(i = 0; i < arrayname.length; i++){
							if (arrayname[i] == value){
								var Found = "Yes";
								var gotopage = eval('romloc'+ary+'[0]');
								var url = new RegExp(gotopage);
								var location = top.location.href;
								if (location.search(url) != -1)
									{var gotopage = eval('romloc'+ary+'[1]');
									top.location.hash = "p"+value;
									setTimeout(top.TEXT.location.hash = "p"+value,500);
									 }
								else{
								// Check Added because IE does not like Timeout and
								//Netscape needs the Timeout
								// to avoid refresh+run problem cause a GPF
								if (navigator.appName == 'Netscape')  {
									setTimeout(top.location.href = gotopage+"#p"+value,20)}
									else{top.location.href = gotopage+"#p"+value;}
                                    
									}
							}
						}
					}

		if(Found == "No"){
			if (value >=1 && value <= 12){
			var Found = "Yes";
			var url = new RegExp("ECINTRFR.HTM");
			var location = top.location.href;
			if (location.search(url) != -1)
				{top.location.hash = "p"+value;
				setTimeout(top.TEXT.location.hash = "p"+value,500);
				}
				else{
					if (navigator.appName == 'Netscape')  {
					setTimeout(top.location.href = "ECINTRFR.HTM#p"+value,20)}
					else{top.location.href = "ECINTRFR.HTM#p"+value;}
					}
			}
}
if(Found == "No")
{alert("There is no such page number.\n" + "Valid pages are i-xx and 1-236.");
Found = "Yes";}
}

function eitem(value)
{
var Found = "No";
var EI1 = new Array(14,17);
var EI2 = new Array(18,19);
var EI3 = new Array(20,21);
var EI4 = new Array();
var EI5 = new Array(23,24);
var EI6 = new Array(25,25);
var EI7 = new Array(26,33);
var EI8 = new Array(34,37);
var EI9 = new Array(38,39);
var EI10 = new Array(40,48);
var EI11 = new Array(50,52);
var EI12 = new Array(53,57);
var EI13 = new Array(58,59);
var EI14 = new Array(60,64);
var EI15 = new Array(65,67);
var EI16 = new Array(68,71);
var EI17 = new Array(72,76);
var EI18 = new Array(80,83);
var EI19 = new Array(84,89);
var EI20 = new Array(89,90);
var EI21 = new Array(91,97);
var EI22 = new Array(98,101);
var EI23 = new Array(102,106);
var EI24 = new Array(107,109);
var EI25 = new Array(110,113);
var EI26 = new Array(114,116);
var EI27 = new Array(117,117);
var EI28 = new Array(118,122);
var EI29 = new Array(124,128);
var EI30 = new Array(129,131);
var EI31 = new Array(132,134);
var EI32 = new Array(135,137);
var EI33 = new Array(138,143);
var EI34 = new Array(144,152);
var EI35 = new Array(155,160);
var EI36 = new Array(161,169);
var EI37 = new Array(170,171);
var EI38 = new Array(172,173);
var EI39 = new Array(174,181);
var EI40 = new Array(182,185);
var EI41 = new Array(186,189);
var EI42 = new Array(190,194);
var EI43 = new Array(195,209);
var EI44 = new Array(210,211);
var EI45 = new Array(213,216);
var EI46 = new Array(217,219);
var EI47 = new Array(220,223);
var EI48 = new Array(224,224);
var EI49 = new Array(225,232);
var EI50 = new Array(233,236);
var rom1 = ['xiii','xiv','xv','xvi'];
var rom2 = ['xvii','xviii','xix','xx'];
var rom3 = ['vii','viii'];
var rom4 = ['ix','x','xi','xii'];
var rom5 = [123];
var rom6 = [153,154];
var rom7 = new Array(1,12);
var rom8 = [237,238];
var rom9 = [22];
var rom10= [212];
var rom11 = [13];
var rom12 = [77,78,79];
var rom13 = [49];
var romloc1 = new Array("EIPREFFR.HTM","EIPREFAC.HTM");
var romloc2 = new Array("EIACK_FR.HTM","EIACK.HTM");
var romloc3 = new Array("EDED_FR.HTM","EDED.HTM");
var romloc4 = new Array("INDEX.HTM","EITOC.HTM");
var romloc5 = new Array("EIMPL_FR.HTM","EIMPLC.HTM");
var romloc6 = new Array("EINHERFR.HTM","EINHERC.HTM");
var romloc7 = new Array("EIINTRFR.HTM","EIINTRO.HTM");
var romloc8 = new Array("EIAFTRFR.HTM","EIAFTER.HTM");
var romloc9 = new Array("EMEM_FR.HTM","EMEMORYC.HTM");
var romloc10 = new Array("EMISC_FR.HTM","MISCC.HTM");
var romloc11 = new Array("ESHIFTFR.HTM","ESHIFTC.HTM");
var romloc12 = new Array("EDESGNFR.HTM","EDESIGNC.HTM");
var romloc13 = new Array("ECTORFR.HTM","ECTORSC.HTM");

var aryname = "EI";
	for (ary = 1; ary <=50 ; ary++)
	{
		var arrayname = eval(aryname+ary);
		if (value >=arrayname[0] && value <= arrayname[1])
				{
					var Found = "Yes";
					var url = new RegExp("EI"+ary+"_FR.HTM");
					var location = top.location.href;
					if (location.search(url) != -1)
						{top.location.hash = "p"+value;
						setTimeout(top.TEXT.location.hash = "p"+value,500);
						
						}
						else
						{
							if (navigator.appName == 'Netscape')  {
							setTimeout(top.location.href = "EI"+ary+"_FR.HTM#p"+value,20)}
							else
							{top.location.href = "EI"+ary+"_FR.HTM#p"+value;}

							}
				}
	}
if (Found == "No"){
	var aryname = "rom";
	for (ary = 1; ary <=13 ; ary++)
	{
		var arrayname = eval(aryname+ary);

			for(i = 0; i < arrayname.length; i++){
				if (arrayname[i] == value)
					{var Found = "Yes";
					var gotopage = eval('romloc'+ary+'[0]');
					var url = new RegExp(gotopage);
					var location = top.location.href;
					if (location.search(url) != -1)
						{var gotopage = eval('romloc'+ary+'[1]');
						top.location.hash = "p"+value;
						setTimeout(top.TEXT.location.hash = "p"+value,500);					 
						 }
				    else{
					if (navigator.appName == 'Netscape')  {
					setTimeout(top.location.href = gotopage+"#p"+value,20)}
					else{top.location.href = gotopage+"#p"+value}

					}
					}
			}
			}
	}
if(Found == "No"){
		if (value >=1 && value <= 12){
				var Found = "Yes";
				var url = new RegExp("EIINTRFR.HTM");
				var location = top.location.href;
				if (location.search(url) != -1)
				   {top.location.hash = "p"+value;
				   setTimeout(top.TEXT.location.hash = "p"+value,500);
				   }
				else{if (navigator.appName == 'Netscape')  {
					setTimeout(top.location.href = "EIINTRFR.HTM#p"+value,20)}
					else {top.location.href = "EIINTRFR.HTM#p"+value}


					}
			}
			
}
if(Found == "No")
{alert("There is no such page number.\n" + "Valid pages are i-xx and 1-236.");
Found = "Yes";}


}


function ebook(value)
{
var Found = "No";
if(value >=1 && value <=236)
{var Found = "Yes";
 var url = new RegExp("E_FR.HTM");
 var location = top.location.href;
 if (location.search(url) != -1)
	{top.location.hash = "p"+value;
	setTimeout(top.TEXT.location.hash = "p"+value,500);
	}
 else{
 if (navigator.appName == 'Netscape')  {
		setTimeout(top.location.href = "E_FR.HTM#p"+value ,20)}
		else
		{top.location.href = "E_FR.HTM#p"+value}
		}
}



var rom1 = ['xiii','xiv','xv','xvi'];
var rom2 = ['xvii','xviii','xix','xx'];
var rom3 = ['vii','viii'];
var rom4 = ['ix','x','xi','xii'];
var romloc1 = new Array("E_FR.HTM","E.HTM");
var romloc2 = new Array("E_FR.HTM","E.HTM");
var romloc3 = new Array("E_FR.HTM","E.HTM");
var romloc4 = new Array("INDEX.HTM","TOC.HTM");

	var aryname = "rom";
	for (ary = 1; ary <=4 ; ary++)
	{
		var arrayname = eval(aryname+ary);

			for(i = 0; i < arrayname.length; i++){
				if (arrayname[i] == value)
					{var Found = "Yes";
					var gotopage = eval('romloc'+ary+'[0]');					
					var url = new RegExp(gotopage);
					var location = top.location.href;
					if (location.search(url) != -1)
						{var gotopage = eval('romloc'+ary+'[1]');
					     top.location.hash = "p"+value;
						 setTimeout(top.TEXT.location.hash = "p"+value,500);
						}
				    else{if (navigator.appName == 'Netscape')  {
					setTimeout(top.location.href = gotopage+"#p"+value,20)}
					else{top.location.href = gotopage+"#p"+value;}
					}
					
				}
				}
			}

if(Found == "No")
{alert("There is no such page number.\n" + "Valid pages are i-xx and 1-236.");
Found = "Yes";}


}





// Copyright 1998 Videomation, Inc. All Rights Reserved. 