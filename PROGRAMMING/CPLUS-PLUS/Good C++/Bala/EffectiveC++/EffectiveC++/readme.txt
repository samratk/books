
                   README file for the Effective C++ CD

                             June 10, 1999


Everything you need to know about using the Effective C++ CD is described
on the CD itself.  To get started, just use your web browser to view the
file INDEX.HTM in the root directory of this CD.  That will load the CD's
Home Page.  From there, click on "Introduction" to read about the CD's
content and special features.

If you have difficulty viewing the CD with your web browser, please make
sure you're using a supported browser.  The supported browsers are Netscape
Navigator 4.0 or higher on Win32, Unix, or Mac (excluding version 4.05, for
details follow the link below); and Microsoft Internet
Explorer 4.0 or higher on Win32. You'll need to enable the use of images,
style sheets, JavaScript, Java, and cookies.


If you're still having trouble viewing the CD, it may be due to a
shortcoming in your browser's support for HTML or JavaScript.  The
Effective C++ CD Support Team maintains a list of platform-specific
problems, as well as suggestions on how to work around those problems.  To
view this list, just point your browser to

  http://www.awl.com/cseng/meyerscd/platform_problems.htm

If you're *still* having trouble viewing the CD, contact us at
ec++cd@awl.com, and we'll try to figure out what's going wrong.

We hope you enjoy the Effective C++ CD, and we especially hope you find it
useful.
