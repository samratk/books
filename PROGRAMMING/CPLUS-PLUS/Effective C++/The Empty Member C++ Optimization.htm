<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0036)http://www.cantrip.org/emptyopt.html -->
<HTML><HEAD><TITLE>The "Empty Member" C++ Optimization</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<META content="MSHTML 6.00.2800.1529" name=GENERATOR></HEAD>
<BODY text=#000008 bgColor=#ffffff>The following article appeared in the August 
'97 "C++ Issue" of Dr. Dobb's Journal. 
<P>
<TABLE>
  <TBODY>
  <TR>
    <TD width="10%"></TD>
    <TD>
      <CENTER>
      <H2>The "Empty Member" C++ Optimization</H2>by <A 
      href="http://www.cantrip.org/">Nathan C. Myers</A> </CENTER>
      <P>The Standard C++ library is filled with useful templates, including 
      extended versions of those found in the award-winning <A 
      href="http://www.sgi.com/tech/stl">STL</A> (See DDJ Mar95). These 
      templates offer great flexibility, yet they are optimized for best 
      performance in the common case. As useful as they are in programs, they 
      are equally useful as examples of effective design, and as a source of 
      inspiration for ways to make your own components as efficient and 
      flexible. 
      <P>Some of the ways that they offer flexibility involve "empty" classes: 
      classes which have no data members. Ideally, these empty classes should 
      consume no space at all. They typically declare typedefs or member 
      functions, and you can replace them with your own classes (which might not 
      be empty) to handle special needs. The default empty class is by far the 
      most commonly used, however, so this case must be optimal so that we do 
      not all pay for the occasional special needs of a few. 
      <P>Due to an unfortunate detail of the language definition (explained 
      fully later), instances of empty classes usually occupy storage. In 
      members of other classes, this space overhead can make otherwise- small 
      objects large enough to be unusable in some applications. If this overhead 
      couldn't be avoided in the construction of the Standard Library, the cost 
      of the library's flexibility would drive away many of its intended users. 
      Optimization techniques used in the Standard library are equally useful in 
      your own code. 
      <P>
      <H3>Empty Member Bloat</H3>
      <P>Here's an example. In the Standard C++ library each STL Container 
      constructor takes, and copies, an allocator argument. Whenever the 
      container needs storage, it asks its allocator member. In this way a user 
      with some special memory to allocate (such as a shared memory region) can 
      define an allocator for it, and pass that to a container constructor, so 
      that container elements will be placed there. In the common case, however, 
      the Standard default allocator is used, which just delegates to the global 
      operator <TT>new</TT>. It is an empty object. 
      <P>Here's simplified code for a possible implementation of these 
      components. In Listing 1, the Standard default allocator, 
      <TT>allocator&lt;&gt;</TT>, only has function members. 
      <P>
      <HR>

      <H4>Listing 1: The Standard Default Allocator</H4><PRE>
  template &lt;class T&gt;
    class <STRONG>allocator</STRONG> {   // an empty class
      . . .
      static T* allocate(size_t n)
        { return (T*) ::operator new(n * sizeof T); }
      . . .
    };

</PRE>
      <HR>

      <P>In Listing 2, the generic list container template keeps a private 
      allocator member, copied from its constructor argument. 
      <P>
      <HR>

      <H4>Listing 2: The Standard Container list&lt;&gt; </H4><PRE>  template &lt;class T, class Alloc = allocator&lt;T&gt; &gt;
    class list {
      . . .
      <STRONG>Alloc alloc_</STRONG>; 
      struct Node { . . . };
      Node* head_;      

     public:
      explicit list(Alloc const&amp; a = Alloc())
        : alloc_(a) { . . . }
      . . .
    };

</PRE>
      <OL>Notes: 
        <LI>The <TT>list&lt;&gt;</TT> constructor is declared "explicit" so that 
        the compiler will not use it as an automatic conversion. (This is [was 
        once] a recent language feature.) 
        <LI>How a <TT>list&lt;&gt;</TT> gets storage for <TT>Node</TT> objects, 
        from an allocator intended to supply storage for <TT>T</TT> objects, is 
        a subject for an upcoming article. [The article never came up. It 
        suffices to note that allocator&lt;U&gt; has a constructor from 
        allocator&lt;T&gt;, for any U, T.] </LI></OL>
      <P>
      <HR>

      <P>The member <TT>list&lt;&gt;::alloc_</TT> occupies, usually, four bytes 
      in the object, even in the default case where <TT>Alloc</TT> is the empty 
      class <TT>allocator&lt;T&gt;</TT>. A few extra bytes for the list object 
      doesn't seem so bad until you imagine a big vector of these lists, as in a 
      hash table. Any extra junk in each list is multiplied, and reduces the 
      number of list headers that fit in a virtual memory page, and in cache. 
      Wasted space makes slower programs, even today; memory latency is much 
      larger, relative to CPU clocks, than ever. 
      <P>
      <H3>Empty Objects</H3>
      <P>How can this overhead be avoided? To see, you need to understand why 
      the overhead is there. The Standard C++ language definition says: 
      <BLOCKQUOTE>A class with an empty sequence of members and base class 
        objects is an empty class. Complete objects and member subobjects of an 
        empty class type shall have nonzero size. </BLOCKQUOTE>
      <P>Why must objects with no member data occupy storage? Consider: <PRE>
  struct Bar { };
  struct Foo {
    struct Bar a[2];
    struct Bar b;
  };
  Foo f;

</PRE>
      <P>What are the addresses of <TT>f.b</TT> and the elements of 
      <TT>f.a[]</TT>? If <TT>sizeof(Bar)</TT> were zero, they might all have the 
      same address. If you were keeping track of separate objects by their 
      addresses, <TT>f.b</TT> and <TT>f.a[0]</TT> would appear to be the same 
      object. The Standard committee chose to finesse these issues by forbidding 
      zero-sized addressable objects. 
      <P>Still, why does an empty member take up so much space (four bytes, in 
      our example)? On all the compilers I know of, <TT>sizeof(Bar)</TT> is 1. 
      However, on most architectures objects must be aligned according to their 
      type. For example, if you declare <PRE>
  struct Baz {
    Bar b;
    int* p;
  };

</PRE>
      <P>on most architectures today, <TT>sizeof(Baz)</TT> is 8. This is because 
      the compiler adds "padding" so that member <TT>Baz::p</TT> doesn't cross a 
      memory word boundary. (See Figure 1a.) 
      <P>
      <HR>

      <H4>Figure 1a:</H4><PRE>
  struct Baz
  +-----------------------------------+
  | +-------+-------+-------+-------+ |
  | | Bar b | XXXXX | XXXXX | XXXXX | |
  | +-------+-------+-------+-------+ |
  | +-------------------------------+ |
  | | int* p                        | |
  | +-------------------------------+ |
  +-----------------------------------+

</PRE>
      <HR>

      <H4>Figure 1b:</H4><PRE>
  struct Baz2
  +-----------------------------------+
  | +-------------------------------+ |
  | | int* p                        | |
  | +-------------------------------+ |
  +-----------------------------------+

</PRE>
      <HR>

      <P>How can you avoid this overhead? The Draft hints, in a footnote: 
      <BLOCKQUOTE>A base class subobject of an empty class type may have zero 
        size. </BLOCKQUOTE>
      <P>In other words, if you declared Baz2 this way, <PRE>
  struct Baz2 : Bar {
    int* p;
  };

</PRE>then a compiler is allowed to reserve zero bytes for the empty base 
      class <TT>Bar</TT>; hence, <TT>sizeof(Baz2)</TT> can be just 4 on most 
      architectures. (See Figure 1b.) 
      <P>Compiler implementers are not _required_ to do this optimization, and 
      many don't, yet. However, you can expect that most Standard-conforming 
      compilers will, because the efficiency of so many components of the 
      Standard library (not only the containers) depends on it. 
      <P>
      <H3>Eliminating Bloat </H3>
      <P>You have found the principle you need to eliminate the space overhead. 
      How can you apply it? Let's consider how you might fix the implementation 
      of the the example, template <TT>list&lt;&gt;</TT>. You could just derive 
      from the allocator, as in Listing 3, 
      <P>
      <HR>

      <H4>Listing 3: A Na�ve Way to Eliminate Bloat</H4><PRE>
  template &lt;class T, class Alloc = allocator&lt;T&gt; &gt;
    class list <STRONG>: private Alloc</STRONG> {
      . . .
      struct Node { . . . };
      Node* head_;      

     public:
      explicit list(Alloc const&amp; a = Alloc())
        : Alloc(a) { . . . }
      . . .
    };

</PRE>
      <HR>

      <P>and it would work, mostly. Code in the <TT>list&lt;&gt;</TT> member 
      functions would get storage by calling "<TT>this-&gt;allocate()</TT>" 
      instead of "<TT>alloc_.allocate()</TT>". However, the Alloc type supplied 
      by the user is allowed to have virtual members, and these could conflict 
      with a derived <TT>list&lt;&gt;</TT> member. (To see this, imagine a 
      private member <TT>void list&lt;&gt;::init()</TT>, and a virtual member 
      <TT>bool Alloc::init()</TT>.) 
      <P>A much better approach is to package the allocator with a 
      <TT>list&lt;&gt;</TT> data member, such as the pointer to the first list 
      node (as in Listing 4), so that the allocator's interface cannot leak out. 

      <P>
      <HR>

      <H4>Listing 4: A Better Way to Eliminate Bloat</H4><PRE>
  template &lt;class T, class Alloc = allocator&lt;T&gt; &gt;
    class list {
      . . .
      struct Node { . . . };
      <STRONG>struct P : public Alloc</STRONG> {
        P(Alloc const&amp; a) : Alloc(a), p(0) { }
        Node* p;
      };
      <STRONG>P</STRONG> head_;
      
     public:
      explicit list(Alloc const&amp; a = Alloc())
        : head_(a) { . . . }
      . . .
    };
</PRE>
      <HR>

      <P>Now, <TT>list&lt;&gt;</TT> members get storage by saying 
      "<TT>head_.allocate()</TT>", and mention the first list element by saying 
      "<TT>head_.p</TT>". This works perfectly, there's no unnecessary overhead 
      of any kind, and users of <TT>list&lt;&gt;</TT> can't tell the difference. 
      Like any good optimization, it makes the implementation a bit messier, but 
      doesn't affect the interface. 
      <P>
      <H3>A Packaged Solution</H3>
      <P>There is still room for improvement. As usual, the improvement involves 
      a template. In Listing 5 we've packaged the technique so that it is easy 
      and clean to use. 
      <P>
      <HR>

      <H4>Listing 5: Packaging the Technique</H4><PRE>
  template &lt;class Base, class Member&gt;
    struct <STRONG>BaseOpt</STRONG> : Base {
      Member m;
      BaseOpt(Base const&amp; b, Member const&amp; mem) 
        : Base(b), m(mem) { }
    };

</PRE>
      <HR>

      <P>Using this template, our <TT>list&lt;&gt;</TT> declaration appears as 
      in Listing 6. 
      <P>
      <HR>

      <H4>Listing 6: The Best Way to Eliminate Bloat</H4><PRE>
  template &lt;class T, class Alloc = allocator&lt;T&gt; &gt;
    class list {
      . . .
      struct Node { . . . };
      BaseOpt&lt;<STRONG>Alloc</STRONG>,Node*&gt; head_;
      
     public:
      explicit list(Alloc const&amp; a = Alloc())
        : head_(a,0) { . . . }
      . . .
    };
</PRE>
      <HR>

      <P>This declaration of <TT>list&lt;&gt;</TT> is no bigger or messier than 
      the unoptimized version we started with. Any other library component 
      (Standard or otherwise) can use <TT>BaseOpt&lt;&gt;</TT> just as easily. 
      The member code is only slightly messier; while it's not immediately 
      obvious what's going on, the declaration of <TT>BaseOpt&lt;&gt;</TT> 
      provides a natural place to document the technique and the reasons for it. 

      <P>It is tempting to add members to <TT>BaseOpt&lt;&gt;</TT>, but that 
      would not improve it: they could conflict with members inherited from the 
      Base parameter, just as in Listing 3. 
      <P>
      <H3>Finally</H3>
      <P>This technique can be used now, under any compiler with sturdy template 
      support. Not all C++ compilers support the empty-base optimization yet, 
      though the Sun, HP, IBM, and Microsoft compilers do, but the technique 
      costs nothing extra even on those that don't. When you get a 
      Standard-conforming compiler, it probably will do the optimization, and if 
      your code uses this technique, it will become more efficient 
      automatically. 
      <P><EM>Fergus Henderson contributed an essential refinement to this 
      technique. </EM>
      <P>
      <HR>

      <P><EM>Update: the latest Borland compiler is able to do the optimization, 
      controlled by a mode switch; it defaults </EM>off<EM> in the current 
      version. Watcom's and Symantec's compilers do the optimization. The 
      Metaware compiler does it when running under OS/2. Gcc-3 does it. Apple's 
      MrCpp does it. Metrowerks's 4.0 compiler does it, but (like IBM's) is a 
      bit too aggressive and can put two subobjects of the same type at the same 
      address; I don't know about later ones. </EM>(Other compilers that do the 
      optimization will be listed here, as I get notified.) 
      <P><EM>A Watcom engineer reports that STL benchmarks ran 30% faster after 
      they implemented the empty-base optimization.</EM> 
      <P><EM>Update again: A whole family of related "empty subobject" 
      optimizations are possible, subject to the ABI specifications a compiler 
      must observe. (Jason Merrill pointed some of these out to me, years back.) 
      For example, consider three struct members of (empty) types A, B, and C, 
      and a fourth non-empty. They may, conformingly, all occupy the same 
      address, as long as they don't have any bases in common with one another 
      or with the containing class. A common gotcha in practice is to have the 
      first (or only) member of a class derived from the same empty base as the 
      class. The compiler has to insert padding so that they two subobjects have 
      different addresses. This actually occurs in iterator adapters that have 
      an interator member, both derived from std::iterator. An 
      incautiously-implemented standard std::reverse_iterator might exhibit this 
      problem.</EM> 
      <P>
      <HR>

      <P><A href="http://www.cantrip.org/">Nathan</A> has worked on the ISO/ANSI 
      <A href="http://www.maths.warwick.ac.uk/c++">C++ Standard</A> since 1993. 
      He designed most of what is in <A 
      href="http://www.cantrip.org/lib-locales.html">Chapter 22</A> of the ISO 
      C++ Standard. His interests include library interface design, low level 
      code efficiency, and Free software. </P></TD>
    <TD width="10%"></TD></TR></TBODY></TABLE>
<P>&nbsp;
<P>Return to the <A href="http://www.cantrip.org/">Cantrip Corpus</A>.<BR>Email 
remarks about this page to <A 
href="mailto:ncm-nospam@cantrip.org">ncm-nospam@cantrip.org</A>.<BR><FONT 
size=-1><A href="http://www.cantrip.org/copyright.html">�</A> Copyright 1997 by 
Nathan C. Myers. All Rights Reserved.<BR>URL: 
&lt;http://www.cantrip.org/emptyopt.html&gt; </FONT></P></BODY></HTML>
