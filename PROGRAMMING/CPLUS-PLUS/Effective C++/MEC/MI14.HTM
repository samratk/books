<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN" "http://www.w3.org/TR/REC-html40/frameset.dtd">
<HTML LANG="EN">
<HEAD>
<TITLE>More Effective C++ | Item 14: Use exception specifications judiciously</TITLE>
<LINK REL=STYLESHEET HREF=../INTRO/ECMEC.CSS>

<SCRIPT LANGUAGE="Javascript" SRC="../JAVA/COOKIE.JS"></SCRIPT>
<SCRIPT LANGUAGE="Javascript">var imagemax = 0; setCurrentMax(0);</SCRIPT>
<SCRIPT LANGUAGE="Javascript" SRC="../JAVA/DINGBATS.JS"></SCRIPT>
<SCRIPT>
var dingbase = "MI14_DIR.HTM";
var dingtext = "Item M14, P";
if (self == top) {
 top.location.replace(dingbase + this.location.hash);
}
</SCRIPT>

</HEAD>
<BODY BGCOLOR="#FFFFFF" TEXT="#000000" ONLOAD="setResize()">
<!-- SectionName = "M14: Use exception specifications judiciously" -->
<A NAME="6011"></A>
<DIV ALIGN="CENTER"><FONT SIZE="-1">Back to <A HREF="./MI13_FR.HTM" TARGET="_top">Item 13: Catch exceptions by reference</A> &nbsp;&nbsp;<BR>&nbsp;&nbsp;Continue to <A HREF="./MI15_FR.HTM" TARGET="_top">Item 15: Understand the costs of exception handling</A></FONT></DIV>


<P><A NAME="dingp1"></A><font ID="mititle">Item 14: &nbsp;Use exception specifications judiciously.</font><SCRIPT>create_link(1);</SCRIPT>
</P>

<A NAME="72138"></A>

<P><A NAME="dingp2"></A><A NAME="66651"></A>There's no denying it: exception specifications have appeal. They make code easier to understand, because they explicitly state what exceptions a function may throw. But they're more than just fancy comments. Compilers are sometimes able to detect inconsistent exception specifications during compilation. Furthermore, if a function throws an exception not listed in its exception specification, that fault is detected at runtime, and the special function <CODE>unexpected</CODE> is automatically invoked. Both as a documentation aid and as an enforcement mechanism for constraints on exception usage, then, exception specifications seem <NOBR>attractive.<SCRIPT>create_link(2);</SCRIPT>
</NOBR></P>

<P><A NAME="dingp3"></A><A NAME="44170"></A>
As is often the case, however, beauty is only skin deep. The default behavior for <CODE>unexpected</CODE> is to call <CODE>terminate</CODE>, and the default behavior for <CODE>terminate</CODE> is to call <CODE>abort</CODE>, so the default behavior for a program with a violated exception specification is to halt. Local variables in active stack frames are not destroyed, because <CODE>abort</CODE> shuts down program execution without performing such cleanup. A violated exception specification is therefore a cataclysmic thing, something that should almost never <NOBR>happen.<SCRIPT>create_link(3);</SCRIPT>
</NOBR></P>    <P><A NAME="dingp4"></A><A NAME="44195"></A>
<A NAME="p73"></A>Unfortunately, it's easy to write functions that make this terrible thing occur. Compilers only <I>partially</I> check exception usage for consistency with exception specifications. What they do not check for &#151; what the <NOBR><FONT COLOR="#FF0000" SIZE="-2"><B>&deg;</B></FONT><A HREF="http://www.awl.com/cseng/cgi-bin/cdquery.pl?name=cstandard" onMouseOver = "self.status = 'The latest publicly available C++ standard'; return true" onMouseOut = "self.status = self.defaultStatus" target="_top">language</NOBR> standard</A> <I>prohibits</I> them from rejecting (though they may issue a warning) &#151; is a call to a function that <I>might</I> violate the exception specification of the function making the <NOBR>call.<SCRIPT>create_link(4);</SCRIPT>
</NOBR></P>    <P><A NAME="dingp5"></A><A NAME="44606"></A>
Consider a declaration for a function <CODE>f1</CODE> that has no exception specification. Such a function may throw any kind of <NOBR>exception:<SCRIPT>create_link(5);</SCRIPT>
</NOBR></P><A NAME="44197"></A>
<UL><PRE>extern void f1();                  // might throw anything
</PRE>
</UL><P><A NAME="44199"></A>
<A NAME="dingp6"></A>Now consider a function <CODE>f2</CODE> that claims, through its exception specification, it will throw only exceptions of type <CODE>int</CODE>:<SCRIPT>create_link(6);</SCRIPT>
</P><A NAME="44201"></A>
<UL><PRE>void f2() throw(int);
</PRE>
</UL><P><A NAME="dingp7"></A><A NAME="44203"></A>
It is perfectly legal C++ for <CODE>f2</CODE> to call <CODE>f1</CODE>, even though <CODE>f1</CODE> might throw an exception that would violate <CODE>f2</CODE>'s exception <NOBR>specification:<SCRIPT>create_link(7);</SCRIPT>
</NOBR></P><A NAME="44204"></A>
<UL><PRE>void f2() throw(int)
{
  ...
  f1();                  // legal even though f1 might throw
                         // something besides an int
  ...
}
</PRE>
</UL><P><A NAME="dingp8"></A><A NAME="44712"></A>
This kind of flexibility is essential if new code with exception specifications is to be integrated with older code lacking such <NOBR>specifications.<SCRIPT>create_link(8);</SCRIPT>
</NOBR></P>    <P><A NAME="dingp9"></A><A NAME="44235"></A>
Because your compilers are content to let you call functions whose exception specifications are inconsistent with those of the routine containing the calls, and because such calls might result in your program's execution being terminated, it's important to write your software in such a way that these kinds of inconsistencies are minimized. A good way to start is to avoid putting exception specifications on templates that take type arguments. Consider this template, which certainly looks as if it couldn't throw any <NOBR>exceptions:<SCRIPT>create_link(9);</SCRIPT>
</NOBR></P><A NAME="44270"></A>
<UL><PRE>// a poorly designed template wrt exception specifications
template&lt;class T&gt;
bool operator==(const T&amp; lhs, const T&amp; rhs) throw()
{
  return &amp;lhs == &amp;rhs;
}
</PRE>
</UL><P><A NAME="dingp10"></A><A NAME="44269"></A>
This template defines an <CODE>operator==</CODE> function for all types. For any pair of objects of the same type, it returns <CODE>true</CODE> if the objects have the same address, otherwise it returns <CODE>false</CODE>.<SCRIPT>create_link(10);</SCRIPT>
</P>    <P><A NAME="dingp11"></A><A NAME="44276"></A>
<A NAME="p74"></A>This template contains an exception specification stating that the functions generated from the template will throw no exceptions. But that's not necessarily true, because it's possible that <CODE>operator&amp;</CODE> (the address-of operator &#151; see <a href="../EC/EI45_FR.HTM#8160" TARGET="_top">Item E45</A>) has been overloaded for some types. If it has, <CODE>operator&amp;</CODE> may throw an exception when called from inside <CODE>operator==</CODE>. If it does, our exception specification is violated, and off to <CODE>unexpected</CODE> we <NOBR>go.<SCRIPT>create_link(11);</SCRIPT>
</NOBR></P>    <P><A NAME="dingp12"></A><A NAME="44299"></A>
This is a specific example of a more general problem, namely, that there is no way to know <I>anything</I> about the exceptions thrown by a template's type parameters. We can almost never provide a meaningful exception specification for a template, because templates almost invariably use their type parameter in some way. The conclusion? Templates and exception specifications don't <NOBR>mix.<SCRIPT>create_link(12);</SCRIPT>
</NOBR></P>    <P><A NAME="dingp13"></A><A NAME="44306"></A>
A second technique you can use to avoid calls to <CODE>unexpected</CODE> is to omit exception specifications on functions making calls to functions that themselves lack exception specifications. This is simple common sense, but there is one case that is easy to forget. That's when allowing users to register callback <NOBR>functions:<SCRIPT>create_link(13);</SCRIPT>
</NOBR></P><A NAME="66726"></A>
<UL><PRE>// Function pointer type for a window system callback
// when a window system event occurs
typedef void (*CallBackPtr)(int eventXLocation,
                            int eventYLocation,
                            void *dataToPassBack);
</PRE>
</UL><A NAME="66727"></A>
<UL><PRE>// Window system class for holding onto callback
// functions registered by window system clients
class CallBack {
public:
  CallBack(CallBackPtr fPtr, void *dataToPassBack)
  : func(fPtr), data(dataToPassBack) {}
</PRE>
</UL><A NAME="66728"></A>
<UL><PRE>  void makeCallBack(int eventXLocation,
                    int eventYLocation) const throw();
</PRE>
</UL><A NAME="66729"></A>
<UL><PRE>private:
  CallBackPtr func;               // function to call when
                                  // callback is made
</PRE>
</UL><A NAME="66784"></A>
<UL><PRE>
   void *data;                       // data to pass to callback
};                                // function
</PRE>
</UL><A NAME="66786"></A>
<UL><PRE>// To implement the callback, we call the registered func-
// tion with event's coordinates and the registered data
void CallBack::makeCallBack(int eventXLocation,
                            int eventYLocation) const throw()
{
  func(eventXLocation, eventYLocation, data);
}
</PRE>
</UL><P><A NAME="dingp14"></A><A NAME="66712"></A>
<A NAME="p75"></A>Here the call to <CODE>func</CODE> in <CODE>makeCallBack</CODE> runs the risk of a violated exception specification, because there is no way of knowing what exceptions <CODE>func</CODE> might <NOBR>throw.<SCRIPT>create_link(14);</SCRIPT>
</NOBR></P>    <P><A NAME="dingp15"></A><A NAME="9589"></A>
This problem can be eliminated by tightening the exception specification in the <CODE>CallBackPtr</CODE> typedef:<a href="#9602"><sup>5</sup></A><SCRIPT>create_link(15);</SCRIPT>
</P><A NAME="9594"></A>
<UL><PRE>
typedef void (*CallBackPtr)(int eventXLocation,
                            int eventYLocation,
                            void *dataToPassBack) throw();
</PRE>
</UL><P><A NAME="dingp16"></A><A NAME="44347"></A>
Given this typedef, it is now an error to register a <CODE>callback</CODE> function that fails to guarantee it throws <NOBR>nothing:<SCRIPT>create_link(16);</SCRIPT>
</NOBR></P><A NAME="44348"></A>
<UL><PRE>
// a callback function without an exception specification
void callBackFcn1(int eventXLocation, int eventYLocation,
                  void *dataToPassBack);
</PRE>
</UL><A NAME="66844"></A>
<UL><PRE>void *callBackData;
</PRE>
</UL><A NAME="66845"></A>
<UL><PRE>...
</PRE>
</UL><A NAME="44349"></A>
<UL><PRE>
CallBack c1(callBackFcn1, callBackData);
                               // error! callBackFcn1
                               // might throw an exception
</PRE>
</UL><A NAME="66825"></A>
<UL><PRE>// a callback function with an exception specification
void callBackFcn2(int eventXLocation,
                  int eventYLocation,
                  void *dataToPassBack) throw();
</PRE>
</UL><A NAME="44354"></A>
<UL><PRE>
CallBack c2(callBackFcn2, callBackData);
                               // okay, callBackFcn2 has a
                               // conforming ex. spec.
</PRE>
</UL><P><A NAME="dingp17"></A><A NAME="44335"></A>
This checking of exception specifications when passing function pointers is a relatively recent addition to the language, so don't be surprised if your compilers don't yet support it. If they don't, it's up to you to ensure you don't make this kind of <NOBR>mistake.<SCRIPT>create_link(17);</SCRIPT>
</NOBR></P>    <P><A NAME="dingp18"></A><A NAME="44367"></A>
A third technique you can use to avoid calls to <CODE>unexpected</CODE> is to handle exceptions "the system" may throw. Of these exceptions, the most common is <CODE>bad_alloc</CODE>, which is thrown by <CODE>operator</CODE> <CODE>new</CODE> and <CODE>operator</CODE> <CODE><NOBR>new[]</NOBR></CODE> when a memory allocation fails (see <A HREF="./MI8_FR.HTM#33985" TARGET="_top">Item 8</A>). If you use the <CODE>new</CODE> operator (again, see <A HREF="./MI8_FR.HTM#33985" TARGET="_top">Item 8</A>) in any function, you must be prepared for the possibility that the function will encounter a <CODE>bad_alloc</CODE> <NOBR>exception.<SCRIPT>create_link(18);</SCRIPT>
</NOBR></P><A NAME="44383"></A>
<P><A NAME="dingp19"></A>
Now, an ounce of prevention may be better than a pound of cure, but sometimes prevention is hard and cure is easy. That is, sometimes it's easier to cope with unexpected exceptions directly than to prevent them from arising in the first place. If, for example, you're writing soft<A NAME="p76"></A>ware that uses exception specifications rigorously, but you're forced to call functions in libraries that don't use exception specifications, it's impractical to prevent unexpected exceptions from arising, because that would require changing the code in the <NOBR>libraries.<SCRIPT>create_link(19);</SCRIPT>
</NOBR></P><A NAME="44398"></A>
<P><A NAME="dingp20"></A>
If preventing unexpected exceptions isn't practical, you can exploit the fact that C++ allows you to replace unexpected exceptions with exceptions of a different type. For example, suppose you'd like all unexpected exceptions to be replaced by <CODE>UnexpectedException</CODE> objects. You can set it up like <NOBR>this,<SCRIPT>create_link(20);</SCRIPT>
</NOBR></P>
<A NAME="82709"></A>
<UL><PRE>
class UnexpectedException {};          // all unexpected exception
                                       // objects will be replaced
                                       // by objects of this type
<A NAME="44396"></A>

void convertUnexpected()               // function to call if
{                                      // an unexpected exception
  throw UnexpectedException();         // is thrown
}
</PRE>
</UL><A NAME="82737"></A>
<P><A NAME="dingp21"></A>and make it happen by <CODE></CODE>replacing the default <CODE>unexpected</CODE> function with <CODE>convertUnexpected</CODE>:<SCRIPT>create_link(21);</SCRIPT>
</P>

<A NAME="44406"></A>
<UL><PRE>set_unexpected(convertUnexpected);
</PRE>
</UL><A NAME="82668"></A>

<P><A NAME="dingp22"></A>
Once you've done this, any unexpected exception results in <CODE>convertUnexpected</CODE> being called. The unexpected exception is then replaced by a new exception of type <CODE>UnexpectedException</CODE>. Provided the exception specification that was violated includes <CODE>UnexpectedException</CODE>, exception propagation will then continue as if the exception specification had always been satisfied. (If the exception specification does not include <CODE>UnexpectedException</CODE>, <CODE>terminate</CODE> will be called, just as if you had never replaced <CODE>unexpected</CODE>.)<SCRIPT>create_link(22);</SCRIPT>
</P><A NAME="82748"></A>
<P><A NAME="dingp23"></A>
Another way to translate unexpected exceptions into a well known type is to rely on the fact that if the <CODE>unexpected</CODE> function's replacement rethrows the current exception, that exception will be replaced by a new exception of the standard type <CODE>bad_exception</CODE>. Here's how you'd arrange for that to <NOBR>happen:<SCRIPT>create_link(23);</SCRIPT>
</NOBR></P>
<A NAME="82761"></A>
<UL><PRE>
void convertUnexpected()          // function to call if
{                                 // an unexpected exception
  throw;                          // is thrown; just rethrow
}                                 // the current exception
<A NAME="82757"></A>
set_unexpected(convertUnexpected);
                                  // install convertUnexpected
                                  // as the unexpected
                                  // replacement
</PRE>
</UL><A NAME="82738"></A>
<A NAME="p77"></A><P><A NAME="dingp24"></A>
If you do this and you include <CODE>bad_exception</CODE> (or its base class, the standard class <CODE>exception</CODE>) in all your exception specifications, you'll never have to worry about your program halting if an unexpected exception is encountered. Instead, any wayward exception will be replaced by a <CODE>bad_exception</CODE>, and that exception will be propagated in the stead of the original <NOBR>one.<SCRIPT>create_link(24);</SCRIPT>
</NOBR></P><A NAME="44438"></A>
<P><A NAME="dingp25"></A>
By now you understand that exception specifications can be a lot of trouble. Compilers perform only partial checks for their consistent usage, they're problematic in templates, they're easy to violate inadvertently, and, by default, they lead to abrupt program termination when they're violated. Exception specifications have another drawback, too, and that's that they result in <CODE>unexpected</CODE> being invoked even when a higher-level caller is prepared to cope with the exception that's arisen. For example, consider this code, which is taken almost verbatim from <A HREF="./MI11_FR.HTM#39749" TARGET="_top">Item 11</A>:<SCRIPT>create_link(25);</SCRIPT>
</P>
<A NAME="44476"></A>
<UL><PRE>
class Session {                  // for modeling online
public:                          // sessions
  ~Session();
  ...
<A NAME="57532"></A>
private:
  static void logDestruction(Session *objAddr) throw();
};
<A NAME="44458"></A>
Session::~Session()
{
  try {
    logDestruction(this);
  }
  catch (...) {  }
}
</PRE>
</UL><A NAME="44456"></A>
<P><A NAME="dingp26"></A>
The <CODE>Session</CODE> destructor calls <CODE>logDestruction</CODE> to record the fact that a <CODE>Session</CODE> object is being destroyed, but it explicitly catches any exceptions that might be thrown by <CODE>logDestruction</CODE>. However, <CODE>logDestruction</CODE> comes with an exception specification asserting that it throws no exceptions. Now, suppose some function called by <CODE>logDestruction</CODE> throws an exception that <CODE>logDestruction</CODE> fails to catch. This isn't supposed to happen, but as we've seen, it isn't difficult to write code that leads to the violation of exception specifications. When this unanticipated exception propagates through <CODE>logDestruction</CODE>, <CODE>unexpected</CODE> will be called, and, by default, that will result in termination of the program. This is correct behavior, to be sure, but is it the behavior the author of <CODE>Session</CODE>'s destructor wanted? That author took pains to handle <I>all possible</I> exceptions, so it seems almost unfair to halt the program without giving <CODE>Session</CODE>'s destructor's <CODE>catch</CODE> block a chance to work. If <CODE>logDestruction</CODE> had no exception specification, <A NAME="p78"></A>this I'm-willing-to-catch-it-if-you'll-just-give-me-a-chance scenario would never arise. (One way to prevent it is to replace <CODE>unexpected</CODE> as described <NOBR>above.)<SCRIPT>create_link(26);</SCRIPT>
</NOBR></P><A NAME="44538"></A>
<P><A NAME="dingp27"></A>
It's important to keep a balanced view of exception specifications. They provide excellent documentation on the kinds of exceptions a function is expected to throw, and for situations in which violating an exception specification is so dire as to justify immediate program termination, they offer that behavior by default. At the same time, they are only partly checked by compilers and they are easy to violate inadvertently. Furthermore, they can prevent high-level exception handlers from dealing with unexpected exceptions, even when they know how to. That being the case, exception specifications are a tool to be applied judiciously. Before adding them to your functions, consider whether the behavior they impart to your software is really the behavior you <NOBR>want.<SCRIPT>create_link(27);</SCRIPT>
</NOBR></P>

<DIV ALIGN="CENTER"><FONT SIZE="-1">Back to <A HREF="./MI13_FR.HTM" TARGET="_top">Item 13: Catch exceptions by reference</A> &nbsp;&nbsp;<BR>&nbsp;&nbsp;Continue to <A HREF="./MI15_FR.HTM" TARGET="_top">Item 15: Understand the costs of exception handling</A></FONT></DIV>

<A NAME="9602"></A>
<HR WIDTH="100%">
<A NAME="dingp28"></A>
<SUP>5</SUP> Alas, it can't, at least not portably. Though many compilers accept the code shown on this page, the <NOBR><FONT COLOR="#FF0000" SIZE="-2"><B>&deg;</B></FONT><A HREF="http://www.awl.com/cseng/cgi-bin/cdquery.pl?name=committee" onMouseOver="self.status='ISO/ANSI Standatdization Committee Home Page'; return true" onMouseOut="self.status=self.defaultStatus" target="_top">standardization</NOBR> committee</A> has inexplicably decreed that "an exception specification shall not appear in a <CODE>typedef</CODE>." I don't know why. If you need a portable solution, you must &#151; it hurts me to write this &#151; make <CODE>CallBackPtr</CODE> a macro, sigh.<SCRIPT>create_link(28);</SCRIPT>

<BR>
<A HREF="#9589">Return</A>

</BODY>
</HTML>
